const config = require('./config/config');
const handler = require('./src/handler').getHandler();

const run = () => {
  const message = handler.greetings(config.environment.runMode);
  console.log(message);
};

run();
