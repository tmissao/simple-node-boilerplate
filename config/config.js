const config = {
  environment: {
    runMode: process.env.RUN_MODE
  }
};

module.exports = config;
