const constants = require('./constants/constants');
const handler = require('./handler').getHandler();

describe('Testing Handler Greetings', () => {
  it('Greetings - Development', () => {
    expect(handler.greetings(constants.RUN_MODE.DEVELOPMENT)).toEqual('What`s up ?');
  });

  it('Greetings - Production', () => {
    expect(handler.greetings(constants.RUN_MODE.PRODUCTION)).toEqual('Hello dear gentleman');
  });

  it('Greetings - None', () => {
    expect(handler.greetings()).toEqual('Hello dear gentleman');
  });
});
