const constants = {
  RUN_MODE: {
    PRODUCTION: 'PRODUCTION',
    DEVELOPMENT: 'DEVELOPMENT'
  }
};

module.exports = constants;
