const constants = require('./constants');

describe('Testing Constants RUN MODE', () => {
  it('PRODUCTION', () => {
    expect(constants.RUN_MODE.PRODUCTION).toEqual('PRODUCTION');
  });

  it('DEVELOPMENT', () => {
    expect(constants.RUN_MODE.DEVELOPMENT).toEqual('DEVELOPMENT');
  });
});
