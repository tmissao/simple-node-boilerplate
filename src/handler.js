const constants = require('./constants/constants');

const _greetings = (runMode) => {
  if (runMode === constants.RUN_MODE.DEVELOPMENT) {
    return 'What`s up ?';
  }

  return 'Hello dear gentleman';
};

const getHandler = () => ({
  greetings: _greetings
});

module.exports.getHandler = getHandler;
